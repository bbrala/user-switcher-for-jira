package com.tngtech.jira.plugins.schizophrenia.rest.model;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "mayuserswitch")
@XmlAccessorType(XmlAccessType.FIELD)
public class MayUserSwitchModel {
    private boolean mayUserSwitch;

    public MayUserSwitchModel(boolean mayUserSwitch) {
        this.mayUserSwitch = mayUserSwitch;
    }
    
    public boolean isMaySwitch() {
        return mayUserSwitch;
    }

    public void setMaySwitch(boolean maySwitch) {
        this.mayUserSwitch = maySwitch;
    }
}
